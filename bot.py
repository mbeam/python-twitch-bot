#########################################
## BETA BETA BETA BETA BETA BETA BETA !##
#########################################

#########  Req Imoports #################
import socket
# from pyautogui import press, typewrite, hotkey
from twitch import TwitchClient
import time
import datetime
import random
# import urllib
# import json
###########    Set Vars    ##############
### Needed for scripts ###
now = datetime.datetime.now()
linecount = 0

### Twitch Settings ###
SERVER = "irc.twitch.tv"
PORT = 6667 
PASS = "[Get oauth key from https://twitchapps.com/tmi/]"
BOT = "[YOUR BOT NAME]"
CHANNEL = "[Channel to Join]"
OWNER = "[Who owns this bot ?]" 
############ Functions ##################
def sendMessage(s, message):
    messageTemp = "PRIVMSG #" + CHANNEL + " :" + message
    s.send((messageTemp + "\r\n").encode())

def getUser(line):
    separate = line.split(":", 2)
    user = separate[1].split("!", 1)[0]
    return user
def getMessage(line):
    global message
    try:
        message = (line.split(":", 2))[2]
    except:
        message = ""
    return message
def joinchat():
    readbuffer_join = "".encode()
    Loading = True
    while Loading:
        readbuffer_join = s.recv(1024)
        readbuffer_join = readbuffer_join.decode()
        temp = readbuffer_join.split("\n")
        readbuffer_join = readbuffer_join.encode()
        readbuffer_join = temp.pop()
        for line in temp:
            Loading = loadingCompleted(line)
    sendMessage(s, "/me entered the room")
    print("Bot active in " + CHANNEL ) 
def loadingCompleted(line):
    if ("End of /NAMES list" in line):
        return False
    else:
        return True
############# Run #######################
s_prep = socket.socket()
s_prep.connect((SERVER, PORT))
s_prep.send(("PASS " + PASS + "\r\n").encode())
s_prep.send(("NICK " + BOT + "\r\n").encode())
s_prep.send(("JOIN #" + CHANNEL + "\r\n").encode())
s = s_prep
joinchat()
readbuffer = ""

def Console(line):
    if "PRIVMSG" in line:
        return False
    else:
        return True
 
while True:
        try:
            readbuffer = s.recv(1024)
            readbuffer = readbuffer.decode()
            temp = readbuffer.split("\n")
            readbuffer = readbuffer.encode()
            readbuffer = temp.pop()
        except:
            temp = ""
        for line in temp:
            if line == "":
                break
            # required to prevent bot time out
            if "PING" in line and Console(line):
                msgg = ("PONG tmi.twitch.tv\r\n".encode())
                s.send(msgg)
                print(msgg)
                # added for random  bot chat activity
                lines = open('rand_message.txt').read().splitlines()
                myline =random.choice(lines)
                sendMessage(s,(myline))
                break
            
            user = getUser(line)
            message = getMessage(line)
            print(user + " -> " + message)
            PMSG = "/w " + user + " "
 
################################# Commands #################################
### Mods only ###

            if user == OWNER and "!bb help" in message:
                time.sleep(3)
                sendMessage(s, "help currently offline")
                break

            if user == OWNER and "!bb emote 60" in message:
                sendMessage(s, "/emoteonly")
                time.sleep(60)
                sendMessage(s, "/emoteonlyoff")
                
            if user == OWNER and "!bb reset showbot" in message:
                sendMessage(s, "ok i'll do it")
                time.sleep(1)
                sendMessage(s, "!sr")
                time.sleep(1)
                sendMessage(s, "there you go " + (user))
                break
  
### Normal users ###
            
            if "!test" in message:
                sendMessage(s, "Your test failed " + (user))
                break
            
            if "who am i" in message:
                sendMessage(s, "Your " + (user))
                break
            
            if "!date" in message:
                sendMessage(s,now.strftime("%m-%d-%Y %H:%M:%S"))
                break

### Private messages ###
            #if "!private" in message:
                #sendMessage(s, PMSG + "You Rang ?")
                #break

################################### END ####################################
